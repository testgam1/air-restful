package com.tistory.gitlab.airrestful.auth;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.mindrot.jbcrypt.BCrypt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * JwtTests
 */
public class SecurityTests {

    @Test
    public void jwt_sign키_생성() {
        //Url safe string
        String jwt = Jwts.builder().setHeaderParam("x-type","JWT")
        .setSubject("sub").signWith(SignatureAlgorithm.HS512, "test-key").compact();
        
        //verification
        Jwts.parser().setSigningKey("test-key").parseClaimsJws(jwt).getBody();
    }

    @Test
    public void bcrypt_암호화테스트(){
        String password = "testPassword";

        String passwordHashed = BCrypt.hashpw(password, BCrypt.gensalt());

        assertThat( BCrypt.checkpw(password, passwordHashed), is(true));
    }
}