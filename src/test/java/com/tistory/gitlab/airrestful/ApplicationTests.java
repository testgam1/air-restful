package com.tistory.gitlab.airrestful;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tistory.gitlab.airrestful.model.BankingUsageInfo;
import com.tistory.gitlab.airrestful.model.Device;
import com.tistory.gitlab.airrestful.repository.BankingUsageInfoRepository;
import com.tistory.gitlab.airrestful.repository.DeviceRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

	@Value("classpath:initial/initial-data.csv")
	Resource initialData;

	@Autowired
	DeviceRepository deviceRepository;

	@Autowired
	BankingUsageInfoRepository bankingUsageInfoRepository;

	/**
	 * Initial Event Clean
	 */
	@Before
	public void initialClean(){
		deviceRepository.deleteAll();
	}
	/**
	 * 테스트 데이터 수가 맞는지를 검증
	 */
	@Test
	public void 테스트데이터_로드_테스트() {
		List<List<String>> csvData = new ArrayList<>();
		List<Device> devices = new ArrayList<Device>();

		try (BufferedReader in = new BufferedReader(
				new InputStreamReader(new FileInputStream(initialData.getFile()), "utf-8"))) {
			String line;
			while ((line = in.readLine()) != null) {
				String[] values = line.split(",");
				csvData.add(Arrays.asList(values));
			}

			
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 분류를 입력
		csvData.get(0).stream().filter(column -> !(column.contains("기간") || column.contains("이용률")))
		.forEach(column -> {
			Device temp = Device.builder().deviceName(column).build();

			devices.add(temp);
		});
		
		// 본문을 입력
		// List<BankingUsageInfo> bankingUsageInfos = new ArrayList<>();
		int rowSize = csvData.size();
		for(int row=1; row < rowSize; row++){
			List<String> currentRow = csvData.get(row);
			int colSize = currentRow.size();
			for(int col=2; col < colSize; col++){
				if("-".equals(currentRow.get(col))) continue;
				
				devices.get(col-2).getBankingUsageInfos().add(BankingUsageInfo.builder()
										.year(Integer.parseInt(currentRow.get(0)))
										.rate(Float.parseFloat(currentRow.get(col)))
										.device(devices.get(col-2))
										.build());

				
		
			}
		}
		deviceRepository.saveAll(devices);
		
		
		//assert
		assertThat(deviceRepository.count(), is(5L));
		assertThat(bankingUsageInfoRepository.count(), is(37L));

	}

}
