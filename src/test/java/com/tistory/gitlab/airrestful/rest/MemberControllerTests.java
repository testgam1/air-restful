package com.tistory.gitlab.airrestful.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tistory.gitlab.airrestful.model.Member;
import com.tistory.gitlab.airrestful.model.MemberDTO;
import com.tistory.gitlab.airrestful.repository.MemberRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * MemberControllerTests
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberControllerTests {
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext ctx;

    @MockBean
    MemberRepository memberRepository;


    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).alwaysDo(print()).build();
    }

    @Test
    public void 멤버가입() throws Exception {
        // given
        MemberDTO memberDto = MemberDTO.builder().userId("testId").password("testPassword").build();
        Member member = Member.of(memberDto);

        when(memberRepository.save(member)).thenReturn(member);

        // when
        ResultActions result = mockMvc
                .perform(post("/member/signup").content(new ObjectMapper().writeValueAsString(memberDto))
                        .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_UTF8));

        // then
        result.andExpect(status().isOk());
    }

    @Test
    public void 멤버로그인() throws Exception {
        //given
        //original member
        MemberDTO memberDto = MemberDTO.builder().userId("testId").password("testPassword").build();
        
        //hashed Member
        String hashed = BCrypt.hashpw("testPassword", BCrypt.gensalt());
        MemberDTO hashedMemberDto = MemberDTO.builder().userId("testId").password(hashed).build();
        Member hashedMember = Member.of(hashedMemberDto);

        when(memberRepository.findByUserId(any())).thenReturn(hashedMember); // 인코딩된 멤버 리턴

        //when
        ResultActions result = mockMvc.perform(post("/member/login").content(
            new ObjectMapper().writeValueAsString(memberDto)
        ).accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_UTF8));

        //then
        result.andExpect(status().isOk());
    }
}