package com.tistory.gitlab.airrestful.rest;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tistory.gitlab.airrestful.model.BankingUsageInfo;
import com.tistory.gitlab.airrestful.model.Device;
import com.tistory.gitlab.airrestful.repository.BankingUsageInfoRepository;
import com.tistory.gitlab.airrestful.repository.BankingUsageInfoRepositorySupport;
import com.tistory.gitlab.airrestful.repository.DeviceRepository;
import com.tistory.gitlab.airrestful.security.JwtInterceptor;
import com.tistory.gitlab.airrestful.service.DeviceService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


/**
 * DeviceControllerTests
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class DeviceControllerTests {
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext ctx;

    @MockBean
    DeviceRepository deviceRepository;

    @MockBean
    BankingUsageInfoRepositorySupport bankingUsageInfoRepositorySupport;

    @MockBean
    BankingUsageInfoRepository bankingUsageInfoRepository;

    @Autowired
    DeviceService deviceService;

    @MockBean
    JwtInterceptor jwtTokenProvider;

    @Before
    public void setUp() throws Exception {
         this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).alwaysDo(print()).build();

         //jwt 인증 무조건 통과
         when(jwtTokenProvider.preHandle(any(), any(), any())).thenReturn(true);
    }

    /**
     * 인터넷뱅킹 서비스 접속 기기 목록
     * @throws Exception
     */
    @Test
    public void 인터넷뱅킹_서비스_접속기기_목록_API() throws Exception {

        //Arrange
        List<Device> devices = new ArrayList<>();
        Device device1 = Device.builder().deviceId("test-id1").deviceName("test-name1").build();
        device1.getBankingUsageInfos().add(BankingUsageInfo.builder().year(1986).rate(10.1f).device(device1).build());
        Device device2 = Device.builder().deviceId("test-id2").deviceName("test-name2").build();
        device2.getBankingUsageInfos().add(BankingUsageInfo.builder().year(2001).rate(20.1f).device(device2).build());
        devices.add(device1);
        devices.add(device2);

        when(deviceRepository.findAll()).thenReturn(devices);
        //Act
        ResultActions result = mockMvc.perform(get("/devices").accept(MediaType.APPLICATION_JSON_VALUE));


        //Assert
        result.andExpect(jsonPath("$.devices",hasSize(2)));
    }

    @Test
    public void 각_년도별_인터넷뱅킹_최다_접속기기_API() throws Exception {
        //given
        List<Device> devices = new ArrayList<>();
        Device device1 = Device.builder().deviceId("test-id1").deviceName("test-name1").build();
        device1.getBankingUsageInfos().add(BankingUsageInfo.builder().year(2001).rate(10.1f).device(device1).build());
        Device device2 = Device.builder().deviceId("test-id2").deviceName("test-name2").build();
        device2.getBankingUsageInfos().add(BankingUsageInfo.builder().year(2001).rate(20.1f).device(device2).build());
        devices.add(device1);
        devices.add(device2);

        List<Integer> years = Arrays.asList(2001,2002);
        
        when(bankingUsageInfoRepositorySupport.getYearList()).thenReturn(years);
        when(bankingUsageInfoRepository.findTopByYearOrderByRateDesc(2001)).thenReturn(device1.getBankingUsageInfos().get(0));
        when(bankingUsageInfoRepository.findTopByYearOrderByRateDesc(2002)).thenReturn(device2.getBankingUsageInfos().get(0));
        
        //when
        ResultActions result = mockMvc.perform(get("/most-used-devices").accept(MediaType.APPLICATION_JSON_VALUE));

        //then
        result.andExpect(jsonPath("$.devices[0].device_id",is("test-id1")));
        result.andExpect(jsonPath("$.devices[1].device_id",is("test-id2")));

    }

    @Test
    public void 특정년도_인터넷뱅킹_최다접속기기_API() throws Exception {
        //given
        List<Device> devices = new ArrayList<>();
        Device device1 = Device.builder().deviceId("test-id1").deviceName("test-name1").build();
        device1.getBankingUsageInfos().add(BankingUsageInfo.builder().year(2001).rate(10.1f).device(device1).build());
        devices.add(device1);

        when(bankingUsageInfoRepository.findTopByYearOrderByRateDesc(2001)).thenReturn(device1.getBankingUsageInfos().get(0));
        
        //when
        ResultActions result = mockMvc.perform(get("/most-used-devices/2001").accept(MediaType.APPLICATION_JSON_VALUE));

        //then
        result.andExpect(jsonPath("$.result.device_name",is("test-name1")));

    }

    @Test
    public void 디바이스아이디별_최다접속년도_API() throws Exception {
        //given
        List<Device> devices = new ArrayList<>();
        Device device1 = Device.builder().deviceId("test-id1").deviceName("test-name1").build();
        device1.getBankingUsageInfos().add(BankingUsageInfo.builder().year(2001).rate(10.1f).device(device1).build());
        device1.getBankingUsageInfos().add(BankingUsageInfo.builder().year(2012).rate(10.5f).device(device1).build());
        devices.add(device1);
        when(bankingUsageInfoRepository.findTopByDeviceDeviceIdOrderByRateDesc("test-id1")).thenReturn(device1.getBankingUsageInfos().get(1));
        
        //when
        ResultActions result = mockMvc.perform(get("/most-used-info-by-device/test-id1").accept(MediaType.APPLICATION_JSON_VALUE));

        //then
        result.andExpect(jsonPath("$.result.year",is(2012)));

    }





    
}