package com.tistory.gitlab.airrestful.repository;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.tistory.gitlab.airrestful.model.BankingUsageInfo;
import com.tistory.gitlab.airrestful.model.Device;
import com.tistory.gitlab.airrestful.model.DeviceDTO;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * RepositorySupportTest
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RepositorySupportTest {

    @Autowired
    BankingUsageInfoRepositorySupport bankingUsageInfoRepositorySupport;

    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    BankingUsageInfoRepository bankingUsageInfoRepository;
    
    /**
     * initial 데이터 제거
     */
    @Before
    public void preClean(){
        deviceRepository.deleteAll();

    }
    
    @Test
    public void 각_년도별_인터넷뱅킹_가장_많이_이용하는_접속기기(){
        //given
        List<Device> devices = new ArrayList<>();
        Device device1 = Device.builder().deviceId("test-id1").deviceName("test-name1").build();
        device1.getBankingUsageInfos().add(BankingUsageInfo.builder().year(2001).rate(10.1f).device(device1).build());
        device1.getBankingUsageInfos().add(BankingUsageInfo.builder().year(2002).rate(1.1f).device(device1).build());
        Device device2 = Device.builder().deviceId("test-id2").deviceName("test-name2").build();
        device2.getBankingUsageInfos().add(BankingUsageInfo.builder().year(2001).rate(20.1f).device(device2).build());
        device2.getBankingUsageInfos().add(BankingUsageInfo.builder().year(2001).rate(0.1f).device(device2).build());
        devices.add(device1);
        devices.add(device2);
    
        deviceRepository.saveAll(devices);
        
        //when
        List<Integer> yearList = bankingUsageInfoRepositorySupport.getYearList();
        List<BankingUsageInfo> infos = yearList.stream().map(year->bankingUsageInfoRepository.findTopByYearOrderByRateDesc(year)).collect(Collectors.toList());
        DeviceDTO.MostUsedDevicesRes res = DeviceDTO.MostUsedDevicesRes.of(infos);

        //then
        assertThat(res.getDevices().size(),is(2));

    }
    
}