package com.tistory.gitlab.airrestful.regression;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.junit.Test;

/**
 * RegressionTest
 */
public class RegressionTest {

    @Test
    public void 선형회귀테스트(){
        double[] year = {2011,2012,2013,2014,2015,2016,2017,2018};
        double[] price = {26.3,33.5,64.3,64.2,73.2,85.1,90.6,90.5};

        SimpleRegression reg = new SimpleRegression();

        for(int i=0; i<year.length; i++){
            reg.addData(year[i], price[i]);

        }

        reg.predict(2019);
    }
}