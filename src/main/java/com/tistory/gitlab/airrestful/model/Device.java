package com.tistory.gitlab.airrestful.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Device
 */
@Entity
@Getter
@NoArgsConstructor
public class Device {
    
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    String deviceId;

    String deviceName;

    
    @OneToMany(mappedBy = "device", cascade = CascadeType.ALL,  orphanRemoval = true)
    List<BankingUsageInfo> bankingUsageInfos = new ArrayList<>();

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @Builder
    public Device(String deviceId, String deviceName){
        this.deviceId=deviceId;
        this.deviceName=deviceName;
    }


}