package com.tistory.gitlab.airrestful.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * BankingUsageInfo
 */
@Entity
@Getter
@NoArgsConstructor
public class BankingUsageInfo {

    @Id
    @GeneratedValue
    long id;
    int year;

    @ManyToOne
    @JoinColumn(name = "device_device_id")
    Device device;
    
    float rate;

    @Builder
    public BankingUsageInfo(int year, Device device, float rate){
        this.year = year;
        this.device = device;
        this.rate = rate;
    }
}