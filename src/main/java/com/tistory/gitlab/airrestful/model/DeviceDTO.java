package com.tistory.gitlab.airrestful.model;

import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.ToString;

/**
 * DeviceWrapper
 */
public class DeviceDTO {

    @Getter
    public static class DevicesRes {
        List<DeviceVo> devices;

        public static DevicesRes of(List<Device> devices){
            DevicesRes res = new DevicesRes();
            res.devices = devices.parallelStream()
                            .map(DeviceVo::new)
                            .collect(Collectors.toList());

            return res;

        }
    }

    @Getter
    public static class DeviceVo {
        String deviceId;
        String deviceName;

        public DeviceVo(Device device){
            this.deviceId=device.deviceId;
            this.deviceName=device.deviceName;
        }
    }

    @Getter
    @ToString
    public static class MostUsedDevicesRes {
        List<UsedInfoVo> devices;

        public static MostUsedDevicesRes of(List<BankingUsageInfo> infos){
            MostUsedDevicesRes res = new MostUsedDevicesRes();
            res.devices = infos.parallelStream()
                            .map(UsedInfoVo::new)
                            .collect(Collectors.toList());

            return res;

        }
    }

    @Getter
    public static class UsedInfoVo {
        int year;
        String deviceId;
        String deviceName;
        float rate;

        public UsedInfoVo(BankingUsageInfo info){
            this.year = info.year;
            this.deviceId = info.device.deviceId;
            this.deviceName = info.device.deviceName;
            this.rate = info.rate;
        }
    }

    @Getter
    public static class DeviceUsedInfoRes {
        DeviceUsedInfo result;

        public static DeviceUsedInfoRes of(BankingUsageInfo info){
            DeviceUsedInfoRes res = new DeviceUsedInfoRes();
            res.result = new DeviceUsedInfo(info);

            return res;
        }
    }

    @Getter
    public static class DeviceUsedInfo {
        int year;
        String deviceName;
        float rate;

        public DeviceUsedInfo(BankingUsageInfo info){
            this.year = info.year;
            this.deviceName = info.device.deviceName;
            this.rate = info.rate;
        }
    }


}