package com.tistory.gitlab.airrestful.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import lombok.Data;

/**
 * Member
 */
@Entity
@Data
public class Member  {

    @Size(min = 4, max = 255)
    @Id
    private String userId;

    @Size(min = 4)
    private String password;

    public static Member of(MemberDTO m){
        Member member = new Member();
        member.setUserId(m.getUserId());
        member.setPassword(m.getPassword());

        return member;
    }
}
