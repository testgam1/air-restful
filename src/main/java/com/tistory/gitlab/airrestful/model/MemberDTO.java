package com.tistory.gitlab.airrestful.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class MemberDTO {
    String userId;
    String password;
}
