package com.tistory.gitlab.airrestful.exception;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * GlobalExceptionHandlerController
 */
@RestControllerAdvice
public class GlobalExceptionHandlerController {
    @ExceptionHandler(CustomHttpException.class)
    public void handleCustomException(HttpServletResponse res, CustomHttpException ex) throws IOException {
      res.sendError(ex.getHttpStatus().value(), ex.getMessage());
    }
  
    
}