package com.tistory.gitlab.airrestful.configuration;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tistory.gitlab.airrestful.model.BankingUsageInfo;
import com.tistory.gitlab.airrestful.model.Device;
import com.tistory.gitlab.airrestful.repository.BankingUsageInfoRepository;
import com.tistory.gitlab.airrestful.repository.DeviceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 * InitialLoadEvent
 */
@Component
public class InitialLoadEvent {
    @Value("${initial.file:classpath:initial/initial-data.csv}")
	Resource initialData;

    @Autowired
	DeviceRepository deviceRepository;

	@Autowired
    BankingUsageInfoRepository bankingUsageInfoRepository;
    
    @EventListener
	public void onApplicationStart(ApplicationReadyEvent event){
		List<List<String>> csvData = new ArrayList<>();
		List<Device> devices = new ArrayList<Device>();

		try (BufferedReader in = new BufferedReader(
				new InputStreamReader(initialData.getInputStream(), "utf-8"))) {
			String line;
			while ((line = in.readLine()) != null) {
				String[] values = line.split(",");
				csvData.add(Arrays.asList(values));
			}

			
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 분류를 입력
		csvData.get(0).stream().filter(column -> !(column.contains("기간") || column.contains("이용률")))
		.forEach(column -> {
			Device temp = Device.builder().deviceName(column).build();

			devices.add(temp);
		});
		
		// 본문을 입력
		int rowSize = csvData.size();
		for(int row=1; row < rowSize; row++){
			List<String> currentRow = csvData.get(row);
			int colSize = currentRow.size();
			for(int col=2; col < colSize; col++){
				if("-".equals(currentRow.get(col))) continue;
				
				devices.get(col-2).getBankingUsageInfos().add(BankingUsageInfo.builder()
										.year(Integer.parseInt(currentRow.get(0)))
										.rate(Float.parseFloat(currentRow.get(col)))
										.device(devices.get(col-2))
										.build());

				
		
			}
		}
		deviceRepository.saveAll(devices);
	}
    
}