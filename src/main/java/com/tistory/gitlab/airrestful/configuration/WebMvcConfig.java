package com.tistory.gitlab.airrestful.configuration;

import com.tistory.gitlab.airrestful.security.JwtInterceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * WebMvcConfig
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Autowired
    private JwtInterceptor jwtInterceptor;

    private static final String[] EXCLUDE_PATHS = { "/member/**", "/error"

    };

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 특정 PATH 를 제외하는 패턴으로 사용한다.
        registry.addInterceptor(jwtInterceptor).addPathPatterns("/**").excludePathPatterns(EXCLUDE_PATHS);
    }

}