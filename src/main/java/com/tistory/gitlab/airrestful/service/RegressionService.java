package com.tistory.gitlab.airrestful.service;

import com.tistory.gitlab.airrestful.model.BankingUsageInfo;
import com.tistory.gitlab.airrestful.model.Device;
import com.tistory.gitlab.airrestful.repository.DeviceRepository;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * RegressionService
 */
@Service
public class RegressionService {
    @Autowired
    DeviceRepository deviceRepository;

    public BankingUsageInfo getEstimate2019Data(String deviceId){
        Device selectedDevice = deviceRepository.findByDeviceId(deviceId);

        SimpleRegression reg = new SimpleRegression();

        selectedDevice.getBankingUsageInfos().forEach(info->{
           reg.addData(info.getYear(), info.getRate());
        });


        return BankingUsageInfo.builder().device(selectedDevice).rate((float)reg.predict(2019)).year(2019).build();
    }
}