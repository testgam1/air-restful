package com.tistory.gitlab.airrestful.service;

import com.tistory.gitlab.airrestful.exception.CustomHttpException;
import com.tistory.gitlab.airrestful.model.Member;
import com.tistory.gitlab.airrestful.repository.MemberRepository;
import com.tistory.gitlab.airrestful.security.JwtTokenProvider;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * MemberService
 */
@Service
public class MemberService {

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    public String signup(Member member) {
        if(memberRepository.findByUserId(member.getUserId()) != null){
            throw new CustomHttpException("dup userId", HttpStatus.BAD_REQUEST);
        }
        member.setPassword(BCrypt.hashpw(member.getPassword(), BCrypt.gensalt()));
        memberRepository.save(member);
        return jwtTokenProvider.createToken(member.getUserId());
    }

    public String signin(Member member) {
        
            Member _member = memberRepository.findByUserId(member.getUserId());
            if(_member == null || !BCrypt.checkpw(member.getPassword(), _member.getPassword()) ){
                throw new CustomHttpException("fail login", HttpStatus.UNAUTHORIZED);
            }
            return jwtTokenProvider.createToken(member.getUserId());
        
    }

}