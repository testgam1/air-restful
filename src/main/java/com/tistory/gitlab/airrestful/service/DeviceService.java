package com.tistory.gitlab.airrestful.service;

import java.util.List;

import com.tistory.gitlab.airrestful.model.BankingUsageInfo;
import com.tistory.gitlab.airrestful.model.Device;
import com.tistory.gitlab.airrestful.repository.BankingUsageInfoRepository;
import com.tistory.gitlab.airrestful.repository.BankingUsageInfoRepositorySupport;
import com.tistory.gitlab.airrestful.repository.DeviceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * DeviceService
 */
@Service
public class DeviceService {

    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    BankingUsageInfoRepositorySupport bankingUsageInfoRepositorySupport;

    @Autowired
    BankingUsageInfoRepository bankingUsageInfoRepository;

    public List<Device> findAllDevices(){
        return deviceRepository.findAll();
    }

    public List<Integer> getYearList(){
        return bankingUsageInfoRepositorySupport.getYearList();
    }


    public BankingUsageInfo getMostUsedDevicePerYear(int year){
        return bankingUsageInfoRepository.findTopByYearOrderByRateDesc(year);
    }

    public BankingUsageInfo getMostUsedInfoByDevice(String deviceId){
        return bankingUsageInfoRepository.findTopByDeviceDeviceIdOrderByRateDesc(deviceId);
    }

}