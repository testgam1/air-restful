package com.tistory.gitlab.airrestful.repository;

import com.tistory.gitlab.airrestful.model.BankingUsageInfo;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * BankingUsageInfoRepository
 */
public interface BankingUsageInfoRepository extends JpaRepository<BankingUsageInfo, Long>{
    BankingUsageInfo findTopByYearOrderByRateDesc(int year);
    BankingUsageInfo findTopByDeviceDeviceIdOrderByRateDesc(String deviceId);
}