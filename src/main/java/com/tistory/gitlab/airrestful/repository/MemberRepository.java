package com.tistory.gitlab.airrestful.repository;

import com.tistory.gitlab.airrestful.model.Member;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * MemberRepository
 */
public interface MemberRepository extends JpaRepository<Member,String> {
    Member findByUserId(String userId);
    
}