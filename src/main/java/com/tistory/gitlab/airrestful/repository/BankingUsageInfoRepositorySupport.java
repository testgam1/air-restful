package com.tistory.gitlab.airrestful.repository;

import static com.tistory.gitlab.airrestful.model.QBankingUsageInfo.bankingUsageInfo;

import java.util.List;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.tistory.gitlab.airrestful.model.BankingUsageInfo;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
/**
 * BankingUsageInfoRepositorySupport
 */
@Repository
public class BankingUsageInfoRepositorySupport extends QuerydslRepositorySupport {

    private final JPAQueryFactory queryFactory;
    
    public BankingUsageInfoRepositorySupport(JPAQueryFactory queryFactory) {
        super(BankingUsageInfo.class);
        this.queryFactory=queryFactory;
    }

    public List<Integer> getYearList(){
        List<Integer> fetch = queryFactory.select(bankingUsageInfo.year)
        .from(bankingUsageInfo)
        .distinct().fetch();
        return fetch;
    }

    
}