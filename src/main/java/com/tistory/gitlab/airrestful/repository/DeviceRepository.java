package com.tistory.gitlab.airrestful.repository;

import com.tistory.gitlab.airrestful.model.Device;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * DeviceRepository
 */

public interface DeviceRepository extends JpaRepository<Device, String> {
    Device findByDeviceId(String deviceId);
}