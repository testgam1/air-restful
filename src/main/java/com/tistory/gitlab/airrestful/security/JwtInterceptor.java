package com.tistory.gitlab.airrestful.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class JwtInterceptor implements HandlerInterceptor {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        
        // 토큰 가져옴
        String token = request.getHeader("Authorization");

        if(token != null){
            boolean isRefresh = false;

            // bearer 일 경우, prefix 제거
            if(token.startsWith("Bearer ")){
                isRefresh = true;
                token = token.substring(7);
            }

            try{
                // 토큰 유효성 검증
                if(jwtTokenProvider.validateToken(token)){
                    if(isRefresh){
                        // bearer 일 경우 재발급
                        response.addHeader("Authorization", jwtTokenProvider.refreshToken(token));
                    }
                    return true;
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        log.debug("token not found");
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        return false;
    }

}
