package com.tistory.gitlab.airrestful.security;

import java.util.Base64;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Setter;

/**
 * JwtTokenProvider
 */
@Component
public class JwtTokenProvider {
    private static String secretKey;
    
    @Value("${pay-test.secret-key}")
    public void setSecretKey(String key){
        secretKey = key;
    }

    @Value("${pay-test.token-ttl}")
    @Setter
    private long validityInSeconds;

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public String createToken(String username) {

        Claims claims = Jwts.claims().setSubject(username);

        Date now = new Date();
        return Jwts.builder().setClaims(claims).setIssuedAt(now)
                .setExpiration(new Date(now.getTime() + validityInSeconds*1000))
                .signWith(SignatureAlgorithm.HS256, secretKey).compact();
    }

    
    public static String getUserId(final String token){
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    } 
    
    public boolean validateToken(String token) {
        
        try{
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            throw e;
        }
    }
    
    public String refreshToken(String token){
        try {
            String userId = JwtTokenProvider.getUserId(token);
            return createToken(userId);
        } catch (JwtException | IllegalArgumentException e) {
            throw e;
        }
    }
}
