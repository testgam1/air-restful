package com.tistory.gitlab.airrestful.rest;

import java.util.List;
import java.util.stream.Collectors;

import com.tistory.gitlab.airrestful.model.BankingUsageInfo;
import com.tistory.gitlab.airrestful.model.Device;
import com.tistory.gitlab.airrestful.model.DeviceDTO;
import com.tistory.gitlab.airrestful.service.DeviceService;
import com.tistory.gitlab.airrestful.service.RegressionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * DeviceContorller
 */
@RestController
public class DeviceController {

    @Autowired
    DeviceService deviceService;

    @Autowired
    RegressionService regressionService;

    /**
     * 인터넷뱅킹 서비스 접속 기기 목록 출력
     * @return
     */
    @GetMapping("/devices")
    public DeviceDTO.DevicesRes getDevices(){
        List<Device> results = deviceService.findAllDevices();

        return DeviceDTO.DevicesRes.of(results);
    }

    /**
     * 특정 년도를 입력받아 그 해에 인터넷뱅킹에 가장 많이 접속하는 기기 이름 출력
     * @param year
     */
    @GetMapping("/most-used-devices/{year}")
    public DeviceDTO.DeviceUsedInfoRes getMostUserDevicesPerYear(@PathVariable Integer year){
        BankingUsageInfo result = deviceService.getMostUsedDevicePerYear(year);

        return DeviceDTO.DeviceUsedInfoRes.of(result);
    }

    /**
     * 각 년도별로 인터넷뱅킹을 가장 많이 이용하는 접속기기를 출력
     */
    @GetMapping("/most-used-devices")
    public DeviceDTO.MostUsedDevicesRes getMostUsedDevices(){
        List<Integer> years = deviceService.getYearList();
        List<BankingUsageInfo> results = years.stream().map(year->deviceService.getMostUsedDevicePerYear(year)).collect(Collectors.toList());

        return DeviceDTO.MostUsedDevicesRes.of(results);
    }

    /**
     * 디바이스 아이디를 입력받아 인터넷뱅킹에 접속 비율이 가장 많은 해를 출력
     * @param deviceId
     * @return
     */
    @GetMapping("/most-used-info-by-device/{deviceId}")
    public DeviceDTO.DeviceUsedInfoRes getMostUsedInfo(@PathVariable String deviceId){
        BankingUsageInfo result = deviceService.getMostUsedInfoByDevice(deviceId);
        return DeviceDTO.DeviceUsedInfoRes.of(result);
    }

    /**
     * 각 deviceId를 넣어서 2019년도의 값을 예측하는 API
     */
    @GetMapping("/devices/{deviceId}/2019_rate")
    public DeviceDTO.DeviceUsedInfoRes get2019Rate(@PathVariable String deviceId){
        BankingUsageInfo result = regressionService.getEstimate2019Data(deviceId);

        return DeviceDTO.DeviceUsedInfoRes.of(result);
    }

}