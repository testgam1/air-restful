package com.tistory.gitlab.airrestful.rest;

import java.util.HashMap;
import java.util.Map;

import com.tistory.gitlab.airrestful.model.Member;
import com.tistory.gitlab.airrestful.model.MemberDTO;
import com.tistory.gitlab.airrestful.service.MemberService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * MemberController
 */
@RestController
@RequestMapping("/member")
public class MemberController {

    @Autowired
    private MemberService memberService;

    /**
     * id, password 가입
     * @param member
     * @return
     */
    @PostMapping("/signup")
    public ResponseEntity<Map<String, String>> signup(@RequestBody MemberDTO member) {
        Map<String, String> result = new HashMap<>();
        result.put("jwt", memberService.signup(Member.of(member)));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * id, password 로그인
     * @param member
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<Map<String, String>> login(@RequestBody MemberDTO member) {
        Map<String, String> result = new HashMap<>();
        result.put("jwt", memberService.signin(Member.of(member)));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}