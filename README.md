# 인터넷뱅킹 이용현황 API

## 개발 프레임워크
|종류|명칭|
|-|-|
|Framework|Spring|
|Library|Spring boot Project|
||jjwt, jbcrypt|
||apache-common-math3|
||lombok|
||jpa, queryDSL|
|Database|embedded-h2|
|IDE|vscode|

## 문제해결 전략
- Csv파일 로드: `ApplicationReadyEvent`를 받아서 구동시 로딩한다. `classpath` 외부 파일을 로딩 할 경우, `application.yml`의 `initial.file`의 주석을 해제하고 경로를 수정한다.
- 예측 알고리즘: `선형 회귀`로 데이터가 선형적으로 증감한다고 가정하고 오차 제곱합을 최소화하는 알고리즘을 사용한다. 학습데이터가 부족하여 결과 신뢰가 낮다.
- JWT 인증: `jjwt` 라이브러리를 사용한다. `interceptor`로 요청을 가로채서 jwt Token 의 유효성을 검증한다. `Authorization header`로 판단한다. refresh token 은 API 요청시 `Authorization: Bearer <유효한 token>`로 요청하여 response Header 를 통해 재발급받는다.
- 테스트 코드는 `mockMvc`를 통해 rest Test를 진행한다.
- 계정의 암호화는 `bcrypt`로 hash 처리한다.

## 빌드 및 실행 방법
빌드 전에는 `QDomain`이 생성되지 않아서 `BankingUsageInfoRepositorySupport.java`에서 오류가 발생할 수 있으니 빌드를 반드시 선행
```
mvn clean package
```
Excutable jar 실행
```
java -jar target/air-restful-0.0.1-SNAPSHOT.jar
```

혹은 Maven plugin을 통해 바로 실행
```
mvn spring-boot:run
```

root URL: `http://localhost:8080`

실행 순서는 /member/signup 으로 jwt token을 받아오고
유효한 jwt token으로 이하 API를 호출하도록 한다.

`/rest-client/device.http` 호출 형식 참고
( [vscode plugin rest-client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) )


## API 테스트 샘플
### 계정 등록
#### 요청
```http
POST http://localhost:8080/member/signup HTTP/1.1
content-type: application/json

{
    "userId":"test"
    ,"password":"pass"
}
```
#### 응답
```json
{
  "jwt": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNTY5MjgxOTQ4LCJleHAiOjE1NjkyODU1NDh9.nJ7YXRp8gGTfR1WetyEjOlqImiNIZ3qvVSQqTaMWTIE"
}
```

### 계정 로그인
#### 요청
```http
POST http://localhost:8080/member/login HTTP/1.1
content-type: application/json

{
    "userId":"test"
    ,"password":"pass"
}
```

#### 응답
```json
{
  "jwt": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNTY5MjgyMDAyLCJleHAiOjE1NjkyODU2MDJ9.VrYAhquIAFwykU0zGWSfk975BWyJ45Jej4_EsFNs_Fc"
}
```

### 인터넷뱅킹 서비스 접속기기 목록
#### 요청
```http
GET http://localhost:8080/devices HTTP/1.1
Authorization: eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNTY5Mjc4MjEzLCJleHAiOjE1NzE4NzAyMTN9.otivIuL19FdQ-fJ2kfGLkQ67tb41krGgqRnx7ltE8zw
```

#### 응답
```json
"devices": [
    {
      "device_id": "a2d1d8c3-80c7-4e1e-af4e-1be1dd8647c5",
      "device_name": "스마트폰"
    },
    ...
```

### 특정년도 인터넷뱅킹 최다접속기기
#### 요청
```http
GET http://localhost:8080/most-used-devices/2017 HTTP/1.1
Authorization: eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNTY5Mjc4MjEzLCJleHAiOjE1NzE4NzAyMTN9.otivIuL19FdQ-fJ2kfGLkQ67tb41krGgqRnx7ltE8zw
```

#### 응답
```json
{
  "result": {
    "year": 2017,
    "device_name": "스마트폰",
    "rate": 90.6
  }
}
```

### 각 년도별 인터넷뱅킹 최다 접속기기
#### 요청
```http
GET http://localhost:8080/most-used-devices HTTP/1.1
Authorization: eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNTY5Mjc4MjEzLCJleHAiOjE1NzE4NzAyMTN9.otivIuL19FdQ-fJ2kfGLkQ67tb41krGgqRnx7ltE8zw

```

#### 응답
```json
"devices": [
    {
      "year": 2011,
      "device_id": "c00e713f-168f-450b-ba17-3f96a18c660a",
      "device_name": "데스크탑 컴퓨터",
      "rate": 95.1
    },
    ...
```

### 디바이스 아이디별 최다접속년도
#### 요청
{{deviceId}} 에 /devices 에서 얻은 값을 넣음
```http

GET http://localhost:8080/most-used-info-by-device/{{deviceId}} HTTP/1.1
Authorization: eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNTY5Mjc4MjEzLCJleHAiOjE1NzE4NzAyMTN9.otivIuL19FdQ-fJ2kfGLkQ67tb41krGgqRnx7ltE8zw

```

#### 응답
```json
{
  "result": {
    "year": 2017,
    "device_name": "스마트폰",
    "rate": 90.6
  }
}
```


### 디바이스 아이디별 2019년 값 예측
#### 요청
```http
GET http://localhost:8080/devices/{{deviceId}}/2019_rate HTTP/1.1
Authorization: eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNTY5Mjc4MjEzLCJleHAiOjE1NzE4NzAyMTN9.otivIuL19FdQ-fJ2kfGLkQ67tb41krGgqRnx7ltE8zw

```

#### 응답
```json
{
  "result": {
    "year": 2019,
    "device_name": "스마트폰",
    "rate": 109.15714
  }
}
```
